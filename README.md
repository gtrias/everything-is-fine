# Everything is OK

![Everything is ok](http://homdor.com/public/anonymous/ickyplaintivefreshwatereel?g=aHR0cHM6Ly9mYXQuZ2Z5Y2F0LmNvbS9JY2t5UGxhaW50aXZlRnJlc2h3YXRlcmVlbC5naWY=)

Everything is ok is just a BDD test runner to ensure all your services are working as expected. Allowing you to make e2e tests aginst production.

## What's included

* [cucumber.js](https://github.com/cucumber/cucumber-js) allows you to write tests to ensure all your production services in [Gherkin syntax](https://docs.cucumber.io/gherkin/).
* [WebdriverIO](http://webdriver.io/) gives you webdriver bindings to test frontend stuff using a browser.

## Inspiration

* [cucumber-boilerplate](https://github.com/webdriverio/cucumber-boilerplate)
